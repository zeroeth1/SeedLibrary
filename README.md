# SeedLibrary Repository Manifest

This README serves as a manifest for the SeedLibrary repository, detailing the files and their purposes within the AI system's architecture.

## File Index
- `proto_genome.proto`: Blueprint for the genome structure and its components.
- `bp_taxonomy.proto`: Defines the structure for the taxonomy system.
- `bp_expander.proto`: Defines the structure for the expander system.
- `ess_specification.yaml`: Describes the framework for generating Enhanced SeedScript (ESS) files, including script segmentation and adaptive shorthand.

## Naming Conventions
- `bp_`: Prefix for blueprint files, detailing the structure and components of the AI system.
- `proto_`: Prefix for protocol buffer files, defining data structures and enums used in the system.

## Repository Structure
For now, all files are maintained in the root directory to facilitate management through the API. A clear naming convention aids in identifying file types and purposes.

## Future Automation
Plans are in place to introduce CI/CD jobs for automatic organization of files into appropriate directories based on naming conventions and rules.

## Documentation
Detailed documentation for each component will be provided to ensure clarity and ease of use for developers and collaborators.

## Version Control and Updates
Versioning and detailed update tracking are essential for maintaining the repository as a reliable source of truth for the AI system's architecture.

## SeedScripts
As part of the development process, custom SeedScripts are generated to assist in organizing and managing repository files, enhancing the AI's understanding and interaction with the system.
